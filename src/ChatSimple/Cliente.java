package ChatSimple;
import java.net.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Scanner;
import org.json.JSONObject;
/**
 *
 * @author gmendez
 */
public class Cliente {
    BufferedReader in;
    PrintWriter out;
    Socket cnx;
    Boolean online = false;
    private static final String MESSAGE_SERVER = "message_server";
    private static final String MESSAGE = "message";
    private static final String USERS_ONLINE = "users_online";;
    
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                (new Cliente()).start();
            }
        });
    }
    void start() {
        Conexion hilo;
        String username = "", message = "";
        Scanner consola = new Scanner(System.in);
        try {
            this.cnx = new Socket("25.52.230.183",4444);

            in = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
            out = new PrintWriter(cnx.getOutputStream(),true);

            hilo = new Conexion(in);
            hilo.start(); //Hilo encargado de lecturas del servidor
            while(!online){
                username = consola.nextLine();
                String pass = consola.nextLine();
                JSONObject request = new JSONObject();
                request.put("type", MESSAGE_SERVER);
                request.put("username", username);
                request.put("password", pass);
                out.println(request.toString());
            }
            while(!message.equals("/salir")){
                System.out.println("escribw un mensaje");
                message = consola.nextLine();
                JSONObject send = new JSONObject();
                send.put("type", MESSAGE);
                send.put("message", message);
                out.println(send.toString());
                //hilo.sendMessage(message);
            }
            hilo.ejecutar = false;
            Thread.sleep(2000);
            this.cnx.close();
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    class Conexion extends Thread {
        public boolean ejecutar = true;
        BufferedReader in;

        public Conexion(BufferedReader in){
            this.in = in;
        }

        @Override
        public void run() {
            String response = "";
            while(ejecutar){
                try {

                    response = in.readLine();
                    if (response != null){
                        receiveMessage(response);
                    }
                } catch (IOException ex) {
                    Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        private void sendMessage(String msg){
            JSONObject message = new JSONObject();
            message.put("type", MESSAGE);
            message.put("message", msg);
            out.println(message.toString());
        }
        private void receiveMessage(String response){
            JSONObject message = new JSONObject(response);
            String type = message.getString("type");
            switch(type){
                case MESSAGE:
                    System.out.println("mensaje normal" + message.getString("message"));
                    break;
                case MESSAGE_SERVER:
                    System.out.println("mensaje login: " + message.getString("message") + " - online : " + message.getBoolean("online"));
                    online = message.getBoolean("online");
                    break;
            }
            
        }
    }
}
