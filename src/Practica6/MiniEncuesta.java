package Practica6;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.Statement;
import java.sql.SQLException;
import java.sql.DriverManager;
import javax.swing.JOptionPane;
import java.lang.NoSuchMethodException;
/**
 * @author Gio Arellano
 */
public class MiniEncuesta extends javax.swing.JFrame {
    //private Conexion conexion = new Conexion();
    private Connection conn = null;
    public MiniEncuesta() {
        initComponents();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        rbWindows = new javax.swing.JRadioButton();
        rbLinux = new javax.swing.JRadioButton();
        rbMac = new javax.swing.JRadioButton();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        cbProg = new javax.swing.JCheckBox();
        cbDis = new javax.swing.JCheckBox();
        cbAdm = new javax.swing.JCheckBox();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        slHoras = new javax.swing.JSlider();
        lbHoras = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Elige un sistema operativo");

        buttonGroup1.add(rbWindows);
        rbWindows.setSelected(true);
        rbWindows.setText("Windows");

        buttonGroup1.add(rbLinux);
        rbLinux.setText("Linux");

        buttonGroup1.add(rbMac);
        rbMac.setText("Mac");

        jLabel2.setText("Elige tu especialidad");

        cbProg.setText("Programación");

        cbDis.setText("Diseño gráfico");

        cbAdm.setText("Administración");

        jLabel3.setText("Horas que dedicas en el ordenador");

        slHoras.setMaximum(24);
        slHoras.setToolTipText("Dezlizar");
        slHoras.setValue(12);
        slHoras.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                slHorasStateChanged(evt);
            }
        });

        lbHoras.setText("12");

        jButton1.setText("Generar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbDis)
                            .addComponent(cbProg)
                            .addComponent(cbAdm))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(6, 6, 6)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(rbLinux)
                                            .addComponent(rbWindows)
                                            .addComponent(rbMac)))
                                    .addComponent(jLabel2))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(45, 45, 45)
                        .addComponent(lbHoras, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(slHoras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(134, 134, 134)
                        .addComponent(jButton1)))
                .addGap(0, 135, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rbWindows)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rbLinux)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rbMac)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbProg)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbDis)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbAdm)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(slHoras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbHoras))
                .addGap(18, 18, 18)
                .addComponent(jButton1)
                .addContainerGap(133, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void slHorasStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_slHorasStateChanged
        this.lbHoras.setText(Integer.toString(this.slHoras.getValue()));
    }//GEN-LAST:event_slHorasStateChanged

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        String so = "";
        if(rbWindows.isSelected())
            so = "Windows";
        else if(rbLinux.isSelected())
            so = "Linux";
        else
            so = "Mac";
        String e1 = cbProg.isSelected() ? "S":"N";
        String e2 = cbDis.isSelected() ? "S":"N";
        String e3 = cbAdm.isSelected() ? "S":"N";
        String cadena = so+","+e1+","+e2+","+e3+","+lbHoras.getText();
        this.guardarResultadoDB(so, e1, e2, e3, slHoras.getValue());
        JOptionPane.showMessageDialog(this, cadena, "Guardado exitosamente", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jButton1ActionPerformed
    private void guardarResultadoDB(String sSisOper, String sProgra, String sDiseno, String sAdmon, int horas) {
        Statement stmt;
        String sInsertStmt;
        sInsertStmt = String.format("INSERT INTO respuestas (sisoper,prog,diseno,admon,horas) VALUES ('%s','%s','%s','%s',%d)", sSisOper, sProgra, sDiseno, sAdmon, horas);
        System.out.println(sInsertStmt);
        try {
            if (conn == null) {
                conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/encuesta?" + "useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&user=encuesta&password=encuesta");
            }
            stmt = conn.createStatement();
            stmt.execute(sInsertStmt);

        } catch (SQLException ex) {
            Logger.getLogger(MiniEncuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /*private void guardarDatos(String so, String e1, String e2, String e3, int hrs){
        String insert = "INSERT INTO respuestas(so, sp_prog, sp_disgraf, sp_admin, hrs) VALUES (?, ?, ?, ?, ?)";
        try{
            PreparedStatement stmt = conexion.obtener().prepareStatement(insert);
            stmt.setString(1, so);
            stmt.setString(2, e1);
            stmt.setString(3, e2);
            stmt.setString(4, e3);
            stmt.setInt(5, hrs);
            int retorno = stmt.executeUpdate();
            if(retorno>0){
                System.out.println("sijala");
            }else{
                System.out.println("nojala");
            }
        }catch(ClassNotFoundException | SQLException e){
            e.printStackTrace();
        }
    }*/

    public static void main(String args[]) throws NoSuchMethodException{
        try{
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
        }catch (Exception ex) {
            // handle the error
            System.out.println(ex.getMessage());
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MiniEncuesta().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JCheckBox cbAdm;
    private javax.swing.JCheckBox cbDis;
    private javax.swing.JCheckBox cbProg;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel lbHoras;
    private javax.swing.JRadioButton rbLinux;
    private javax.swing.JRadioButton rbMac;
    private javax.swing.JRadioButton rbWindows;
    private javax.swing.JSlider slHoras;
    // End of variables declaration//GEN-END:variables
}
